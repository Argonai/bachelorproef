import Vapor

func routes(_ app: Application) throws {
    try app.register(collection: ItemsController())

    app.get("hello") { req async -> String in
        "Hello, world!"
    }
}
