
import Foundation
import Vapor
public struct ItemResponse: Content {
    var id: Int
    var name: String
    var description: String
}
extension ItemResponse: AsyncResponseEncodable, Decodable, Equatable {} 
