import Foundation
import Vapor
public struct Item: Content {
    var id: Int
    var name: String
}
extension Item: AsyncResponseEncodable, Decodable, Equatable {} 
