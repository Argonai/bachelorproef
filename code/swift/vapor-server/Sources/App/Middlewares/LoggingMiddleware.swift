import Vapor

struct LoggingMiddleware: Middleware {
    func respond(to request: Request, chainingTo next: Responder) -> EventLoopFuture<Response> {
        request.logger.info("\(request.method): \(request.url) ")
        return next.respond(to: request)
    }
}
