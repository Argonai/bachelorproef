import FluentKit
import Hummingbird
import HummingbirdAuth
import HummingbirdFluent
import NIOPosix

struct BasicAuthenticator<Context: AuthRequestContext>: AuthenticatorMiddleware, @unchecked Sendable {
    func authenticate(request: Request, context: Context) async throws -> User? {
        guard let basic = request.headers.basic else {
           throw HTTPError(.unauthorized) 
            }
        
        if(basic.username == "foo" && basic.password == "bar"){
            return User(name: basic.username, password: basic.password)
        }else {
           throw HTTPError(.unauthorized) 
        }

    }
}
