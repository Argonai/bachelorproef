import ArgumentParser
import Hummingbird
import HummingbirdHTTP2
import Logging
import HummingbirdAuth
import NIOSSL

@main
struct hummingbird_server: AsyncParsableCommand {
    func run() async throws {
        var hostname: String = "0.0.0.0"
        var port: Int = 3000 
        let cert: String = "../../certs/cert.pem"
        let key: String = "../../certs/key.pem"
        let logger = Logger(label: "Hummingbird")
        let router = Router(context: BasicAuthRequestContext.self)
        var tlsConfiguration: TLSConfiguration {
        get throws {
            let certificateChain = try NIOSSLCertificate.fromPEMFile(cert)
            let privateKey = try NIOSSLPrivateKey(file: key, format: .pem)
            return TLSConfiguration.makeServerConfiguration(
                certificateChain: certificateChain.map { .certificate($0) },
                privateKey: .privateKey(privateKey)
            )
        }
    }

        router.middlewares.add(LogRequestsMiddleware(.info))
        ItemsController().addRoutes(to: router.group("/"))
        router.middlewares.add(BasicAuthenticator())
        let app = Application(
        router: router,
        server: try .http2Upgrade(tlsConfiguration: tlsConfiguration),
        configuration: .init(address: .hostname(hostname, port: port)),
        logger: logger
        )
        try await app.runService()
    }
}
