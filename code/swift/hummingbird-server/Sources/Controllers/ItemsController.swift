import Hummingbird
import Foundation
import HummingbirdAuth

public struct ItemsController<Context: AuthRequestContext> {

    func addRoutes(to group: RouterGroup<Context>) {
        group.group("/").add(middleware: BasicAuthenticator())
        .get("", use: getDefault)
    }
    @Sendable func getDefault(request: Request, context: some RequestContext) async throws -> Item {
        return Item(id: 1, name: "Name")
    }
}
