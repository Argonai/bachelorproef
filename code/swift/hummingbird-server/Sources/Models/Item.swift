import Foundation
import Hummingbird

public struct Item {
    var id: Int
    var name: String
}

extension Item: ResponseEncodable, Decodable, Equatable {}
