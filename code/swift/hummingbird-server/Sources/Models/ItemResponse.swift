import Foundation
import Hummingbird

public struct ItemResponse {
    var id: Int
    var name: String
    var description: String
}

extension ItemResponse: ResponseEncodable, Decodable, Equatable {}
