const std = @import("std");

pub fn main() !void {
    var a: *i32 = undefined;
    {
        var b: i32 = 10;
        a = &b;
        std.debug.print("address of b is: {s}\n", .{&b});
    }
    std.debug.print("a is pointing to: {s}\n", .{a});
    const c: i32 = a.*;
    std.debug.print("{d}\n", .{c});
    a.* = 11;
    std.debug.print("{d}\n", .{a.*});
}
