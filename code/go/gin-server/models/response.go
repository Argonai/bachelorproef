package models

type BootNotificationResponse struct {
	CustomData struct {
		VendorID string `json:"vendorId"`
	} `json:"customData"`
	CurrentTime string `json:"currentTime"`
	Interval    int    `json:"interval"`
	Status      string `json:"status"`
	StatusInfo  struct {
		CustomData struct {
			VendorID string `json:"vendorId"`
		} `json:"customData"`
		ReasonCode     string `json:"reasonCode"`
		AdditionalInfo string `json:"additionalInfo"`
	} `json:"statusInfo"`
	MessageType string `json:"messageType"`
	ID          int    `json:"id"`
	Action      string `json:"action"`
}
