package controllers

import (
	"database/sql"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"gitlab.com/argonai/bachelorproef/code/go/gin-server/conn"
	"gitlab.com/argonai/bachelorproef/code/go/gin-server/models"
)

var db *sql.DB
var ws *websocket.Conn

func Register_Routes(g *gin.Engine) {
	authorized := g.Group("/", gin.BasicAuth(gin.Accounts{
		"foo": "bar", // user:foo password:bar
	}))

	authorized.GET("/", index)
	authorized.POST("/boot", boot)

}

func index(g *gin.Context) {
	db = conn.GetDB()
	var id int
	err := db.QueryRow("SELECT 1").Scan(&id)
	if err != nil {
		// Handle the error
		panic(err)
	}
	g.JSON(http.StatusOK, models.Item{Id: id, Name: "Name"})

}

func boot(g *gin.Context) {
	ws = conn.GetWs()

	// Define the JSON payload
	payload := []byte(`[
			2,
			"19223201",
			"BootNotification",
			{
				"customData": {
					"vendorId": "string"
				},
				"chargingStation": {
					"customData": {
						"vendorId": "string"
					},
					"serialNumber": "string",
					"model": "string",
					"modem": {
						"customData": {
							"vendorId": "string"
						},
						"iccid": "string",
						"imsi": "string"
					},
					"vendorName": "string",
					"firmwareVersion": "string"
				},
				"reason": "ApplicationReset"
			}
		]`)
	ws.WriteMessage(websocket.TextMessage, []byte(payload))
	// Send the HTTP POST request
	var response models.BootNotificationResponse
	ws.ReadJSON(response)
	g.JSON(http.StatusOK, response)
}
