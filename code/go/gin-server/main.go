package main

import (
	"database/sql"
	"log"

	"github.com/gin-gonic/gin"
	_ "github.com/mattn/go-sqlite3"
	conn "gitlab.com/argonai/bachelorproef/code/go/gin-server/conn"
	controller "gitlab.com/argonai/bachelorproef/code/go/gin-server/controllers"
)

var Db *sql.DB

func setupRouter() *gin.Engine {
	// Disable Console Color
	// gin.DisableConsoleColor()

	gin.SetMode(gin.ReleaseMode)
	r := gin.Default()
	r.UseH2C = true

	controller.Register_Routes(r)

	return r
}

func main() {
	conn.SetupDB()
	conn.SetupWs()
	r := setupRouter()
	r.Use()
	// Listen and Server in 0.0.0.0:8080
	log.Fatal(r.RunTLS("0.0.0.0:3000", "../../certs/cert.pem", "../../certs/key.pem"))
}
