package conn

import (
	"net/http"

	"github.com/gorilla/websocket"
	_ "github.com/mattn/go-sqlite3"
)

var ws *websocket.Conn

func SetupWs() {
	ws, _, err = websocket.DefaultDialer.Dial("ws://0.0.0.0:9000", http.Header{"Sec-WebSocket-Protocol": []string{"ocpp2.0.1"}})
}

func GetWs() *websocket.Conn {
	return ws
}
