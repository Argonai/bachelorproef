package controllers

import (
	"database/sql"
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	"gitlab.com/argonai/bachelorproef/code/go/gmux-server/conn"
	"gitlab.com/argonai/bachelorproef/code/go/gmux-server/models"
)

var db *sql.DB
var ws *websocket.Conn

func Register_routes(r *mux.Router) {
	r.HandleFunc("/", index)
	r.HandleFunc("/boot", boot)
}

func index(w http.ResponseWriter, r *http.Request) {
	db = conn.GetDB()
	result, err := db.Query("Select 1")
	w.Header().Set("Content-Type", "application/json")
	defer result.Close() // Close the result set after using it
	if err != nil {
		// Handle the error
		println(err)
	}
	if result.Next() {
		var id int
		err = result.Scan(&id)
		if err != nil {
			// Handle the error
		}
		json.NewEncoder(w).Encode(models.Item{
			Id:   id,
			Name: "Name",
		})
	} else {
		// Handle the case when no rows are returned
	}
}

func boot(w http.ResponseWriter, r *http.Request) {
	ws = conn.GetWs()

	// Define the JSON payload
	payload := []byte(`[
			2,
			"19223201",
			"BootNotification",
			{
				"customData": {
					"vendorId": "string"
				},
				"chargingStation": {
					"customData": {
						"vendorId": "string"
					},
					"serialNumber": "string",
					"model": "string",
					"modem": {
						"customData": {
							"vendorId": "string"
						},
						"iccid": "string",
						"imsi": "string"
					},
					"vendorName": "string",
					"firmwareVersion": "string"
				},
				"reason": "ApplicationReset"
			}
		]`)
	ws.WriteMessage(websocket.TextMessage, []byte(payload))
	// Send the HTTP POST request
	var response models.BootNotificationResponse
	ws.ReadJSON(response)
	json.NewEncoder(w).Encode(response)
}
