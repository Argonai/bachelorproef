module gitlab.com/argonai/bachelorproef/code/go/gmux-server

go 1.22.0

require (
	github.com/felixge/httpsnoop v1.0.4 // indirect
	github.com/gorilla/handlers v1.5.2 // indirect
	github.com/gorilla/mux v1.8.1 // indirect
	github.com/gorilla/websocket v1.5.3
	github.com/mattn/go-sqlite3 v1.14.22
)
