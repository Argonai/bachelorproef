package models

type ItemResponse struct {
  Id  int 
  Name string
  Description string
}
