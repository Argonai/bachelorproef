package main

import (
	"crypto/tls"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/argonai/bachelorproef/code/go/gmux-server/conn"
	controller "gitlab.com/argonai/bachelorproef/code/go/gmux-server/controllers"
	middlewares "gitlab.com/argonai/bachelorproef/code/go/gmux-server/middlewares"
)

func main() {
	conn.SetupDB()
	conn.SetupWs()
	r := mux.NewRouter()
	controller.Register_routes(r)
	r.Use(middlewares.LoggingMiddleware)
	r.Use(middlewares.AuthenticationMiddleware)
	tlsConfig := &tls.Config{}
	tlsConfig.NextProtos = []string{"h2"}
	srv := &http.Server{
		Addr:      ":3000",
		Handler:   r,
		TLSConfig: tlsConfig,
	}

	log.Fatal(srv.ListenAndServeTLS(
		"../../certs/cert.pem",
		"../../certs/key.pem",
	))
}
