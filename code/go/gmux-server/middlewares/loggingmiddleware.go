package middlewares

import (
	"fmt"
	"log"
	"net/http"
)

func LoggingMiddleware(next http.Handler) http.Handler {
  return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
    log.Println(fmt.Sprintf("%s: %s", r.Method, r.URL.Path))
    next.ServeHTTP(w,r)
  })
}
