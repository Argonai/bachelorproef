package middlewares

import "net/http"

func AuthenticationMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		user, pass, _ := r.BasicAuth()
		if user == "foo" && pass == "bar" {
			next.ServeHTTP(w, r)
		} else {
			http.Error(w, "Unauthorized.", 401)
			return
		}
	})
}
