mod middlewares;
mod models;
mod routes;
use std::{net::SocketAddr, path::PathBuf, sync::Arc};

use axum::{
    extract::FromRef, middleware, routing::{get, post}, Router
};
use axum_server::tls_rustls::RustlsConfig;
use r2d2::Pool;
use r2d2_sqlite::SqliteConnectionManager;
use routes::routes::{boot, index};
use tokio::sync::Mutex;
use tower::ServiceBuilder;
use tower_http::trace::TraceLayer;
use websockets::WebSocket;
use models::appstate::AppState;
use crate::{middlewares::basicauth};

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::DEBUG)
        .init();
    let config = RustlsConfig::from_pem_file(
        PathBuf::from("../../certs/cert.pem"),
        PathBuf::from("../../certs/key.pem"),
    )
    .await
    .unwrap();
let mut ws = WebSocket::builder()
.add_subprotocol("ocpp2.0.1")
.connect("ws://0.0.0.0:9000")
.await
.expect("Cant connect");
let shared = Arc::new(Mutex::new(ws));
let manager = SqliteConnectionManager::file("database.db");
let pool = Pool::new(manager).unwrap();
let state = AppState {
    ws: shared.clone(),
    pool: pool.clone(),
};
// Connect to SQLite database

    let app = Router::new()
        .route("/", get(index))
        .route("/boot", post(boot))
        .layer(
            ServiceBuilder::new()
                .layer(TraceLayer::new_for_http())
                .layer(middleware::from_fn(basicauth::auth_middleware)),
        )
        .with_state(state)
        ;
    let addr = SocketAddr::from(([0,0,0,0], 3000));
    tracing::debug!("listening on https://0.0.0.0:3000");
    axum_server::bind_rustls(addr, config).serve(app.into_make_service())
    .await
    .unwrap();
}
