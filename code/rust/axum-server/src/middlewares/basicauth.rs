use axum::{
    extract::Request,
    http::{HeaderMap, StatusCode},
    middleware::Next,
    response::Response,
};
use base64::{prelude::BASE64_STANDARD, Engine};
use core::str;

pub async fn auth_middleware(
    headers: HeaderMap,
    request: Request,
    next: Next,
) -> Result<Response, StatusCode> {
    match headers.get("Authorization") {
        Some(token) => {
            let val: Vec<&str> = str::from_utf8(token.as_bytes())
                .unwrap_or("err")
                .split_whitespace()
                .collect();
            let decoded = BASE64_STANDARD.decode(val[1]).unwrap_or(b"err".to_vec());
            let decoded_string: Vec<&str> = str::from_utf8(&decoded).unwrap_or("err").split(":").collect();
            if val[0] == "Basic" && decoded_string[0] == "foo" && decoded_string[1] == "bar" {
                let response = next.run(request).await;
                return Ok(response);
            } else {
                return Err(StatusCode::UNAUTHORIZED);
            }
        }
        _ => return Err(StatusCode::UNAUTHORIZED),
    }
}
