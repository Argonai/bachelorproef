use serde::{Deserialize, Serialize};


#[derive(Serialize, Deserialize)]
pub struct ItemResponse {
    pub id: i32,
    pub name: String,
    pub description: String,
}
