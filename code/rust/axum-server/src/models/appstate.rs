use std::sync::Arc;

use axum::extract::FromRef;
use r2d2::Pool;
use r2d2_sqlite::SqliteConnectionManager;
use tokio::sync::Mutex;
use websockets::WebSocket;

#[derive(Clone)]
pub struct AppState {
    // that holds some api specific state
    pub ws: Arc<Mutex<WebSocket>>,
    pub pool: Pool<SqliteConnectionManager>,
}

impl FromRef<AppState> for Arc<Mutex<WebSocket>> {
    fn from_ref(app_state: &AppState) -> Arc<Mutex<WebSocket>> {
        app_state.ws.clone()
    }
}
impl FromRef<AppState> for Pool<SqliteConnectionManager> {
    fn from_ref(app_state: &AppState) -> Pool<SqliteConnectionManager> {
        app_state.pool.clone()
    }
}