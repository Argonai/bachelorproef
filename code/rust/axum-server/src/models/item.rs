use serde::{Deserialize, Serialize};


#[derive(Serialize, Deserialize)]
pub struct Item {
    pub id: i32,
    pub name: String,
}
