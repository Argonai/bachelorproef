use std::{fs::File, io::BufReader, sync::Arc};

use actix_web::{
    middleware,
    web::{self, Data},
    App, HttpServer,
};
use actix_web_httpauth::middleware::HttpAuthentication;
use middlewares::authmiddleware::basic_auth;
use r2d2::Pool;
use r2d2_sqlite::SqliteConnectionManager;
use tokio::sync::Mutex;
use tokio::time::{self, Duration};
use websockets::{Frame, WebSocket, WebSocketBuilder};
mod middlewares;
mod models;
mod routes;

#[tokio::main]
async fn main() -> Result<(), std::io::Error> {
    env_logger::init_from_env(env_logger::Env::new().default_filter_or("info"));
    let mut certs_file = BufReader::new(File::open("../../certs/cert.pem").unwrap());
    let mut key_file = BufReader::new(File::open("../../certs/key.pem").unwrap());
    let tls_certs = rustls_pemfile::certs(&mut certs_file)
        .collect::<Result<Vec<_>, _>>()
        .unwrap();
    let tls_key = rustls_pemfile::pkcs8_private_keys(&mut key_file)
        .next()
        .unwrap()
        .unwrap();
    let tls_config = rustls::ServerConfig::builder()
        .with_no_client_auth()
        .with_single_cert(tls_certs, rustls::pki_types::PrivateKeyDer::Pkcs8(tls_key))
        .unwrap();
    let mut ws = WebSocket::builder()
        .add_subprotocol("ocpp2.0.1")
        .connect("ws://0.0.0.0:9000")
        .await
        .expect("Cant connect");
    let shared = Arc::new(Mutex::new(ws));

    // Connect to SQLite database
    let manager = SqliteConnectionManager::file("database.db");
    let pool = Pool::new(manager).unwrap();

    HttpServer::new(move || {
        App::new()
            .wrap(HttpAuthentication::basic(basic_auth))
            .wrap(middleware::Logger::default())
            .app_data(web::Data::new(shared.clone()))
            .app_data(web::Data::new(pool.clone())) // Add SQLite connection to app data
            .service(routes::routes::index)
            .service(routes::routes::boot)
    })
    .bind_rustls_0_22(("0.0.0.0", 3000), tls_config)?
    .run()
    .await
}
