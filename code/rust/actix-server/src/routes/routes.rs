use std::sync::Arc;

use crate::models::{item::Item, itemresponse::ItemResponse};
use actix_web::{
    get, post,
    web::{Data, Json},
    HttpRequest, Responder, Result,
};
use r2d2::Pool;
use r2d2_sqlite::SqliteConnectionManager;
use rust_ocpp::v2_0_1::messages::boot_notification::{BootNotificationRequest, BootNotificationResponse};
use rust_ocpp::v2_0_1::messages::heartbeat::HeartbeatRequest;
use tokio::sync::Mutex;
use websockets::{Frame, WebSocket};
use rusqlite::{params, Connection};

#[get("/")]
pub async fn index(_req: HttpRequest, pool: Data<Pool<SqliteConnectionManager>>) -> Result<impl Responder> {
        let conn = pool.get().expect("error with getting connection"); 
        let mut stmt = conn.prepare("SELECT 1").expect("SQL error");
        let id: i32 = stmt.query_row(params![], |row| row.get(0)).expect("SQL error");

        let item = Item {
            id,
            name: "Name".to_string(),
        };

        Ok(Json(item))

}
#[post("/boot")]
pub async fn boot(_req: HttpRequest, ws: Data<Arc<Mutex<WebSocket>>>) -> Result<impl Responder> {
    let message = BootNotificationRequest {
        reason: rust_ocpp::v2_0_1::enumerations::boot_reason_enum_type::BootReasonEnumType::PowerUp,
        charging_station:
            rust_ocpp::v2_0_1::datatypes::charging_station_type::ChargingStationType {
                serial_number: Some("12345".to_string()),
                model: "FakeModel".to_string(),
                vendor_name: "Rpi".to_string(),
                firmware_version: Some("0.1".to_string()),
                modem: Some(rust_ocpp::v2_0_1::datatypes::modem_type::ModemType {
                    iccid: Some("12345".to_string()),
                    imsi: Some("12345".to_string()),
                }),
            },
    };
    let modem = message.charging_station.modem.clone().unwrap();
    let json = serde_json::json!([
        2,
        "19223201",
        "BootNotification",
        {
            "customData": {
                "vendorId": "string"
            },
            "chargingStation": {
                "customData": {
                    "vendorId": "string"
                },
                "serialNumber": message.charging_station.serial_number.unwrap(),
                "model": message.charging_station.model,
                "modem": {
                    "customData": {
                        "vendorId": "string"
                    },
                    "iccid": modem.iccid.unwrap(),
                    "imsi": modem.imsi.unwrap()
                },
                "vendorName": message.charging_station.vendor_name,
                "firmwareVersion": message.charging_station.firmware_version.unwrap()
            },
            "reason": "ApplicationReset"
        }
    ]);
    let mut locked_ws = ws.lock().await;
    let frame = Frame::text(json.to_string()).set_fin(true).set_continuation(false);
    locked_ws.send(frame).await.unwrap();
    let response = locked_ws.receive().await.unwrap();
    drop(locked_ws); // Explicitly drop the lock
    let (response_text,_,_) = response.as_text().unwrap();
    let response : BootNotificationResponse = serde_json::from_str(response_text.as_str()).unwrap();
    Ok(Json(response))
}
