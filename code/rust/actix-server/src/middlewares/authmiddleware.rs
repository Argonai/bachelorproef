use actix_web::{dev::ServiceRequest, error::ErrorUnauthorized, Error};
use actix_web_httpauth::extractors::basic::BasicAuth;

pub async fn basic_auth(req: ServiceRequest, creds: BasicAuth) -> Result<ServiceRequest, (Error, ServiceRequest)> {
    if creds.user_id() == "foo" && creds.password() == Some("bar") {
        Ok(req)
    } else {
        Err((ErrorUnauthorized("Not Authorized"), req))
    }
}
