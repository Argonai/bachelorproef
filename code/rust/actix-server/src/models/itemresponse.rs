use serde::Serialize;


#[derive(Serialize)]
pub struct ItemResponse {
    pub id: i32,
    pub name: String,
    pub description: String
}
